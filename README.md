# README #

### Getting started ###
* I am using Visual Studio 2013 Community Edition (it's free, but you need the WP Deveopment bits installed)
* You may need to refresh the nuget packages and rebuild
* Try with test PasswordSafe in OneDrive of test account - see Passwords.txt
* The UI is rough still, but happy path works

### Basic idea is ###
* Prompt new user to locate their PasswordSafe file - currently this is via OneDrive only, but will be other buttons for other providers like DropBox?
* Run them through the cloud provider authentication (OAuth, etc.)
* Do a search for .psafe3 files on that provider (OneDrive enables this, hopefully the others will as well)
* Have the user select the one they want
* Download that from cloud to local device
* Prompt user for master password.
* On success, prompt user to set a PIN.  This is considering the master password is likely longish and complexish that would be inconvenient to retype on a mobile device every time.  So essentially change the master password for the locally stored file for convenience.
* On pin set and confirmed, the safe is opened and displayed in a panorama (swipe left/right between groups and up/down for password items within those groups)
* Selecting an item presents buttons to copy username, copy password, and copy password and open url if url is set

### Currently this is read-only ###

I’m going to start looking into DropBox for you and will need to consider how to tweak things to be Cloud provider agnostic/pluggable.  Also there is currently no way to load a different safe or update the content after successfully logging into one.

I also need to check the version of the MS Live API I’m using for OneDrive access to see if there is anything newer/better.  I have two DropBox api’s I’m looking at.

﻿using DropNetRT;
using DropNetRT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;

namespace DropBoxTest
{
    class Program
    {
        /*
            Client ID: ???
            Client secret: ???
         */

        
        [STAThread]
        static void Main(string[] args)
        {
            const string key = "pc703mka8s0l59q";
            const string secret = "72h8br0b06pswcc";
            
            var client = new DropNetClient(key, secret);

            var awaiting = true;
            client.GetRequestToken().ContinueWith((task) =>
            {
                if (task.Exception != null)
                {
                    throw task.Exception;
                }

                awaiting = false;
            });
            while (awaiting)
            {
                Thread.Sleep(1);
            }
            awaiting = true;

            var authUriString = client.BuildAuthorizeUrl(client.UserLogin);

            bool permissionSubmitted = false;
            const string permissionSubmissionUriFragment = "authorize_submit";

            var browser = new BrowserWindow();
            browser.Navigating += (sender, eventArgs) =>
            { 
                Console.WriteLine("Navigating: " + eventArgs.Uri);
                if (eventArgs.Uri.OriginalString.Contains(permissionSubmissionUriFragment))
                {
                    permissionSubmitted = true;
                }
            };
            bool allowed = false;
            browser.Navigated += (sender, eventArgs) =>
            {
                /*
                 * Navigating: https://api.dropbox.com/1/oauth/authorize?oauth_token=iQyP84WjNvzEC8my
                 * Navigating: https://www.dropbox.com/1/oauth/authorize?oauth_token=iQyP84WjNvzEC8my
                 * Navigated: https://www.dropbox.com/1/oauth/authorize?oauth_token=iQyP84WjNvzEC8my
                 * 
                 * Navigating: https://www.dropbox.com/1/oauth/authorize_submit
                 * 
                 * (correct + allow)
                 * Navigated: https://www.dropbox.com/1/oauth/authorize_submit
                 * 
                 * (correct + deny)
                 * Navigating: https://www.dropbox.com/
                 * Navigating: https://www.dropbox.com/home
                 * Navigated: https://www.dropbox.com/home
                 */
                var uri = eventArgs.Uri.OriginalString;
                if (permissionSubmitted) {
                    if (uri.Contains(permissionSubmissionUriFragment))
                    {
                        allowed = true;
                    }
                    browser.Close();
                }
                Console.WriteLine("Navigated: " + eventArgs.Uri);
            };

            browser.Show(authUriString);

            if (!allowed)
            {
                Console.WriteLine("Permission denied");
                //Console.WriteLine("Done!");
                Console.ReadKey();
                return;
            }

            var accessToken = client.GetAccessToken().ContinueWith(task => {
                awaiting = false;
            });
            while (awaiting)
            {
                Thread.Sleep(1);
            }
            awaiting = true;

            client = new DropNetClient(key, secret, client.UserLogin.Token, client.UserLogin.Secret);

            List<Metadata> matches = new List<Metadata>();
            client.Search(".psafe3").ContinueWith(task =>
            {
                if (task.Exception != null)
                {
                    throw task.Exception;
                }

                matches = task.Result;
                awaiting = false;
            });
            while (awaiting)
            {
                Thread.Sleep(1);
            }

            foreach (var match in matches)
            {
                Console.WriteLine(match.Name);
            }

            var first = matches[0];

            Console.WriteLine("Done!");
            Console.ReadKey();
        }
    }
}

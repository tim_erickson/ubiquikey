﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using OneDriveRestAPI;
using OneDriveRestAPI.Model;

namespace OneDriveTest
{
    class Program
    {
        /*
            Client ID: 00000000401527B4
            Client secret (v1): bOYKS5RjYeOJYjfQwyacDtPhgJpEEAqO
         */

        [STAThread]
        static void Main(string[] args)
        {
            const string clientId = "00000000401527B4";
            const string clientSecret = "bOYKS5RjYeOJYjfQwyacDtPhgJpEEAqO";
            const string redirectUri = "http://ubiquikey.in2bits.com/redirect";
            
            var scopes = new[] { Scope.Signin, Scope.Basic, Scope.SkyDrive };

            var options = new Options
            {
                ClientId = clientId,
                ClientSecret = clientSecret,
                CallbackUrl = redirectUri,

                AutoRefreshTokens = true,
                PrettyJson = false,
                ReadRequestsPerSecond = 2,
                WriteRequestsPerSecond = 2
            };

            var client = new Client(options);

            string code = null;

            var authUriString = client.GetAuthorizationRequestUrl(scopes);

            var app = new Application();
            var window = new Window();
            var browser = new WebBrowser();
            window.Content = browser;
            browser.Loaded += (sender, eventArgs) => browser.Navigate(authUriString);
            browser.Navigating += (sender, eventArgs) => Console.WriteLine("Navigating: " + eventArgs.Uri);
            browser.Navigated += (sender, eventArgs) =>
                {
                    var uri = eventArgs.Uri.OriginalString;
                    if (uri.Contains("code="))
                    {
                        code = uri.Split(new[] { "code=" }, StringSplitOptions.None)[1];
                        code = code.Split(new[] { "&lc=" }, StringSplitOptions.None)[0];
                        Console.WriteLine("Authorized: " + code);
                        window.Close();
                        app.Shutdown();
                    }
                    else
                    {
                        Console.WriteLine("Navigated: " + eventArgs.Uri);
                    }
                };

            app.Run(window);

            var loggingIn = true;
            UserToken token = null;
            client.GetAccessTokenAsync(code).ContinueWith(task =>
                {
                    loggingIn = false;

                    token = task.Result;
                });
            while (loggingIn)
            {
                Thread.Sleep(1);
            }

            options.AccessToken = token.Access_Token;
            options.RefreshToken = token.Refresh_Token;

            client = new Client(options);

            var getting = true;
            IEnumerable<File> matches = null;
            client.SearchAsync("*.psafe3").ContinueWith(task =>
                {
                    matches = task.Result;
                    //matches = result.data;
                    getting = false;
                });
            while (getting)
            {
                Thread.Sleep(1);
            }

            foreach (var match in matches)
            {
                Console.WriteLine(match.Name);
            }

            Console.WriteLine("Done!");
            Console.ReadKey();
        }
    }
}

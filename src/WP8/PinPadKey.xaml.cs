﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace UbiquiKey
{
    public partial class PinPadKey : Button
    {
        public static readonly DependencyProperty CharProperty = DependencyProperty.Register(
            "Char", typeof (string), typeof (PinPadKey), new PropertyMetadata(CharPropertyChanged));

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(
            "Text", typeof (string), typeof (PinPadKey), new PropertyMetadata(TextPropertyChanged));

        private static void TextPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var instance = (PinPadKey)d;
            if (instance.TextText == null)
                return;
            instance.TextText.Text = (string)e.NewValue;
        }

        private static void CharPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var instance = (PinPadKey)d;
            if (instance.CharText == null)
                return;
            instance.CharText.Text = (string)e.NewValue;
        }

        private TextBlock TextText;
        private TextBlock CharText;

        public PinPadKey()
        {
            InitializeComponent();
        }

        public string Char
        {
            get { return (string) GetValue(CharProperty); }
            set { SetValue(CharProperty, value); }
        }

        public string Text
        {
            get { return (string) GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            TextText = (TextBlock) GetTemplateChild("TextText");
            CharText = (TextBlock) GetTemplateChild("CharText");

            TextText.Text = Text;
            CharText.Text = Char;
        }
    }
}

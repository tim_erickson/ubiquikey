﻿using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using UbiquiKey.Annotations;

namespace UbiquiKey
{
    public class SecurePage : PhoneApplicationPage, INotifyPropertyChanged
    {
        public static PhoneApplicationFrame GetPhoneApplicationFrame()
        {
            var frame = Application.Current.RootVisual as PhoneApplicationFrame;
            if (frame == null)
                throw new ApplicationException("Could not locate PhoneApplicationFrame");
            return frame;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

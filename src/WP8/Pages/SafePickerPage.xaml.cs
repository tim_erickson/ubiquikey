﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using UbiquiKey.Models;
using System.Threading.Tasks;
using System.ComponentModel;
using UbiquiKey.Annotations;
using System.Runtime.CompilerServices;

namespace UbiquiKey.Pages
{
    public partial class SafePickerPage : SecurePage
    {
        private static ISafeProvider SafeProvider;

        public SafePickerPage()
        {
            App.TrackView("SafePickerPage-" + SafeProvider.GetName());

            InitializeComponent();

            DataContext = this;
        }

        private static readonly Uri Uri = new Uri(String.Format("/Pages/{0}.xaml", typeof(SafePickerPage).Name), UriKind.Relative);

        async protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.NavigationMode != NavigationMode.Back)
                await LoadSafeFiles();
        }

        private ObservableCollection<ISafeInfo> _dbFiles = new ObservableCollection<ISafeInfo>();
        public ObservableCollection<ISafeInfo> DbFiles
        {
            get { return _dbFiles; }
            set { }
        }

        private Visibility _progressBarVisibility;
        public Visibility ProgressBarVisibility
        {
            get { return _progressBarVisibility; }
            set
            {
                if (_progressBarVisibility == value)
                {
                    return;
                }
                _progressBarVisibility = value;
                OnPropertyChanged();
            }
        }

        async private Task LoadSafeFiles()
        {
            _dbFiles.Clear();
            await SafeProvider.Initialize(NavigationService);
            var safes = await SafeProvider.SearchForSafes();

            ProgressBarVisibility = System.Windows.Visibility.Collapsed;

            foreach (var safe in safes)
                _dbFiles.Add(safe);

            if (!_dbFiles.Any())
            {
                MessageBox.Show("No Password Safe (.psafe3) files found!");
                return;
            }
        }

        static TaskCompletionSource<ISafeInfo> CompletionSource { get; set; }

        async public static Task<ISafeInfo> Open(NavigationService navigationService, ISafeProvider safeProvider)
        {
            SafeProvider = safeProvider;
            CompletionSource = new TaskCompletionSource<ISafeInfo>();
            navigationService.Navigate(Uri);
            return await CompletionSource.Task;
        }

        private void Database_OnClick(object sender, RoutedEventArgs e)
        {
            App.TrackEvent("ui-event", "pick-safe", SafeProvider.GetName(), 0);
            var safeInfo = ((sender as FrameworkElement).DataContext as ISafeInfo);
            NavigationService.GoBack();
            CompletionSource.SetResult(safeInfo);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Microsoft.Live;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PasswordSafe;
using PasswordSafe.Crypto;
using UbiquiKey.Annotations;
using UbiquiKey.Models;
using Newtonsoft.Json;

namespace UbiquiKey.Pages
{
    public partial class SafeOpenerPage : SecurePage
    {
        public SafeOpenerPage()
        {
            InitializeComponent();

            DataContext = this;
        }

        private static readonly Uri Uri = new Uri(String.Format("/Pages/{0}.xaml", typeof(SafeOpenerPage).Name), UriKind.Relative);
        public static void NavigateTo()
        {
            GetPhoneApplicationFrame().Navigate(Uri);
        }

        private enum States
        {
            FindOrCreate,
            EnterPasskey,
            CreatePin,
            IncorrectPin,
            ConfirmPin
        }

        ISafeInfo SafeInfo { get; set; }
        ISafeProvider SafeProvider { get; set; }

        private string _passphrase;
        public string Passphrase
        {
            get { return _passphrase; }
            set
            {
                if (string.Equals(value, _passphrase))
                    return;
                _passphrase = value;
                OnPropertyChanged();
            }
        }

        private string _dbName;
        public string DbName
        {
            get { return _dbName; }
            set
            {
                if (string.Equals(value, _dbName))
                    return;
                _dbName = value;
                OnPropertyChanged();
            }
        }

        private ICommand _enterPassphraseCommand;
        public ICommand EnterPassphraseCommand
        {
            get { return _enterPassphraseCommand ?? (_enterPassphraseCommand = new SimpleDelegateCommand(ExecuteEnterPassPhraseCommand)); }
        }

        private Safe _rawSafe = null;

        async private void ExecuteEnterPassPhraseCommand(object parameter)
        {
            try
            {
                using (var safeStream = await SafeProvider.GetStream(SafeInfo))
                {
                    if (safeStream == null)
                    {
                        MessageBox.Show("There was a problem retrieving your safe.  Please try again.");
                        return;
                    }
                    else
                    {
                        try
                        {
                            _rawSafe = Safe.Load(new PasswordSafeCrypto(), safeStream, Passphrase);
                        }
                        catch (InvalidPasskeyException)
                        {
                            MessageBox.Show("Wrong passphrase. Please try again.");
                            return;
                        }
                        catch
                        {
                            MessageBox.Show("There was a problem opening your safe. Please try again.");
                            return;
                        }
                        GoTo(States.CreatePin);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("There was a problem retrieving your safe.  Please try again. [" + e.Message + "]");
            }
        }

        private string _confirmingPin;
        public string ConfirmingPin
        {
            get { return _confirmingPin; }
            set
            {
                if (string.Equals(value, _confirmingPin))
                    return;
                _confirmingPin = value;
                OnPropertyChanged();
            }
        }

        private string _pin;
        public string Pin
        {
            get { return _pin; }
            set
            {
                if (string.Equals(value, _pin))
                    return;
                _pin = value;
                OnPropertyChanged();
            }
        }

        private ICommand _enterPinCommand;
        public ICommand EnterPinCommand
        {
            get { return _enterPinCommand ?? (_enterPinCommand = new SimpleDelegateCommand(ExecuteEnterPinCommand)); }
            set { }
        }

        private async void ExecuteEnterPinCommand(object obj)
        {
            if (ConfirmingPin == null)
            {
                ConfirmingPin = Pin;
                GoTo(States.ConfirmPin);
                return;
            }

            if (Pin == ConfirmingPin)
            {
                using (var ms = new MemoryStream())
                {
                    _rawSafe.Save(ms, Pin);
                    ms.Position = 0;
                    var rawBytes = new byte[ms.Length];
                    await ms.ReadAsync(rawBytes, 0, rawBytes.Length);
                    var protectedBytes = ProtectedData.Protect(rawBytes, null);
                    using (var streamToSave = new MemoryStream(protectedBytes))
                    using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
                    using (var stream = storage.OpenFile(SafeInfo.Name, FileMode.Create, FileAccess.ReadWrite))
                        await streamToSave.CopyToAsync(stream);
                }
                App.LocalSafeFile = SafeInfo.Name;
                App.Safe = _rawSafe;
                DatabasePage.NavigateTo();
            }
            else
            {
                MessageBox.Show("PINs don't match.  Try again or tap Back to try a different PasswordSafe file.");
                Pin = null;
            }
        }

        async private void FindSafeOnOneDrive_OnClick(object sender, RoutedEventArgs e)
        {
            App.TrackEvent("ui-event", "open-safe", "OneDrive", 0);
            await GetDbFileInfo(SafeProviders.OneDrive);
        }

        async private void FindSafeOnDropBox_OnClick(object sender, RoutedEventArgs e)
        {
            App.TrackEvent("ui-event", "open-safe", "DropBox", 0);
            await GetDbFileInfo(SafeProviders.DropBox);
        }

        async private Task GetDbFileInfo(ISafeProvider safeProvider)
        {
            SafeProvider = safeProvider;
            SafeInfo = await SafeProvider.FindSafe(NavigationService);
            DbName = SafeInfo.Name;
            GoTo(States.EnterPasskey);
        }

        private void GoTo(States state)
        {
            switch (state)
            {
                case States.FindOrCreate:
                    PassphrasePanelVisibility = Visibility.Collapsed;
                    DbNameVisibility = Visibility.Collapsed;
                    PinPadPaneVisibility = Visibility.Collapsed;
                    break;
                case States.EnterPasskey:
                    PassphrasePanelVisibility = Visibility.Visible;
                    DbNameVisibility = Visibility.Visible;
                    PinPadPaneVisibility = Visibility.Collapsed;
                    break;
                case States.CreatePin:
                    PassphrasePanelVisibility = Visibility.Collapsed;
                    DbNameVisibility = Visibility.Visible;
                    PinPadPaneVisibility = Visibility.Visible;
                    EnterPinPrompt = "create a PIN";
                    break;
                case States.IncorrectPin:
                    PassphrasePanelVisibility = Visibility.Collapsed;
                    DbNameVisibility = Visibility.Visible;
                    PinPadPaneVisibility = Visibility.Visible;
                    break;
                case States.ConfirmPin:
                    PassphrasePanelVisibility = Visibility.Collapsed;
                    DbNameVisibility = Visibility.Visible;
                    PinPadPaneVisibility = Visibility.Visible;
                    EnterPinPrompt = "confirm your PIN";
                    Pin = null;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("state");
            }
        }

        private Visibility _passphrasePanelVisibility = Visibility.Collapsed;
        public Visibility PassphrasePanelVisibility
        {
            get { return _passphrasePanelVisibility; }
            set
            {
                if (value == _passphrasePanelVisibility)
                    return;
                _passphrasePanelVisibility = value;
                OnPropertyChanged();
            }
        }

        private Visibility _dbNameVisibility = Visibility.Collapsed;
        public Visibility DbNameVisibility
        {
            get { return _dbNameVisibility; }
            set
            {
                if (value == _dbNameVisibility)
                    return;
                _dbNameVisibility = value;
                OnPropertyChanged();
            }
        }

        private Visibility _pinPadPaneVisibility = Visibility.Collapsed;
        public Visibility PinPadPaneVisibility
        {
            get { return _pinPadPaneVisibility; }
            set
            {
                if (value == _pinPadPaneVisibility)
                    return;
                _pinPadPaneVisibility = value;
                OnPropertyChanged();
            }
        }

        private string _enterPinPrompt;
        public string EnterPinPrompt
        {
            get { return _enterPinPrompt; }
            set
            {
                if (string.Equals(value, _enterPinPrompt))
                    return;
                _enterPinPrompt = value;
                OnPropertyChanged();
            }
        }
    }
}

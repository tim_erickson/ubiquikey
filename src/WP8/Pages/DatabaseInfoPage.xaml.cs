﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace UbiquiKey.Pages
{
    public partial class DatabaseInfoPage : SecurePage
    {
        public DatabaseInfoPage()
        {
            InitializeComponent();
        }

        private static readonly Uri Uri = new Uri(String.Format("/Pages/{0}.xaml", typeof(DatabaseInfoPage).Name), UriKind.Relative);
        public static void NavigateTo()
        {
            GetPhoneApplicationFrame().Navigate(Uri);
        }
    }
}
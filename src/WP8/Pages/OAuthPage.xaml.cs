﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Diagnostics;

namespace UbiquiKey.Pages
{
    public partial class OAuthPage : SecurePage, IOAuthUI
    {
        public OAuthPage()
        {
            InitializeComponent();

            this.Loaded += OAuthPage_Loaded;

            Handler.RegisterOAuthUI(this);
        }

        private static readonly Uri Uri = new Uri(String.Format("/Pages/{0}.xaml", typeof(OAuthPage).Name), UriKind.Relative);
        public static void NavigateTo()
        {
            GetPhoneApplicationFrame().Navigate(Uri);
        }

        private static IOAuthHandler Handler;
        private static Uri AuthUri;

        public static void Show(NavigationService navigationService, IOAuthHandler handler, Uri authUri)
        {
            Handler = handler;
            AuthUri = authUri;
            OAuthPage.NavigateTo();
        }

        void IOAuthUI.Close()
        {
            NavigationService.GoBack();
            Handler = null;
            AuthUri = null;
        }

        void OAuthPage_Loaded(object sender, RoutedEventArgs e)
        {
            Browser.Navigate(AuthUri);
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            //e.Cancel = true;
        }

        private void Browser_Navigating(object sender, NavigatingEventArgs e)
        {
            Debug.WriteLine("NAVIGATING: " + e.Uri);
            if (Handler != null)
                Handler.Navigating(e.Uri);
        }

        private void Browser_Navigated(object sender, NavigationEventArgs e)
        {
            Debug.WriteLine("NAVIGATED: " + e.Uri);
            if (Handler != null)
                Handler.Navigated(e.Uri);
        }

        private void Browser_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            Debug.WriteLine("NAVIGATION FAILED: " + e.Uri);
            if (Handler != null)
                Handler.NavigationFailed(e.Uri);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PasswordSafe;
using UbiquiKey.Resources;
using System.ComponentModel;
using UbiquiKey.ViewModels;
using System.Collections.ObjectModel;
using System.IO;

namespace UbiquiKey.Pages
{
    public partial class DatabasePage : SecurePage
    {
        public DatabasePage()
        {
            App.TrackView("DatabasePage");

            InitializeComponent();

            BuildApplicationBar();
        }

        private static readonly Uri Uri = new Uri(String.Format("/Pages/{0}.xaml", typeof(DatabasePage).Name), UriKind.Relative);
        public static void NavigateTo()
        {
            GetPhoneApplicationFrame().Navigate(Uri);
        }

        private bool _loaded;

        private readonly List<LongListSelectorGroup<DatabaseItemViewModel>> _groupedItems = new List<LongListSelectorGroup<DatabaseItemViewModel>>();
        private readonly ObservableCollection<LongListSelectorGroup<DatabaseItemViewModel>> _filteredGroupedItems = new ObservableCollection<LongListSelectorGroup<DatabaseItemViewModel>>();
        public ObservableCollection<LongListSelectorGroup<DatabaseItemViewModel>> FilteredGroupedItems { get { return _filteredGroupedItems; } }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.NavigationMode == NavigationMode.Back)
            {
                return;
            }

            DataContext = this;

            PopulateGroups();
        }

        public string DatabaseTitle { get { return String.IsNullOrWhiteSpace(App.Safe.Name) ? Path.GetFileNameWithoutExtension(App.LocalSafeFile) : App.Safe.Name; } }

        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if (String.Equals(_searchText, value))
                    return;
                _searchText = value;
                OnPropertyChanged();
                Filter();
            }
        }

        private void SearchText_TextChanged(object sender, TextChangedEventArgs e)
        {
            //http://stackoverflow.com/questions/4833100/updatesourcetrigger-propertychanged-equivalent-for-a-windows-phone-7-textbox
            TextBox tb = sender as TextBox;
            tb.GetBindingExpression(TextBox.TextProperty).UpdateSource();
        }

        private const string EmptyGroupName = "(empty)";

        private void PopulateGroups()
        {
            var lists = new Dictionary<string, List<DatabaseItemViewModel>>();
            foreach (var item in App.Safe.Items)
            {
                var groupName = item.Group ?? EmptyGroupName;
                if (!lists.ContainsKey(groupName))
                {
                    lists.Add(groupName, new List<DatabaseItemViewModel>());
                }
                lists[groupName].Add(new DatabaseItemViewModel(item));
            }

            foreach (var groupName in lists.Keys)
            {
                var list = lists[groupName];
                list.Sort(new DatabaseItemViewModel.TitleComparer());
                _groupedItems.Add(new LongListSelectorGroup<DatabaseItemViewModel>(groupName, list));
            }

            Filter();
        }

        private void Filter()
        {
            var str = SearchText;
            var filteredGroupIndex = 0;
            foreach (var group in _groupedItems) {
                var filteredGroup = _filteredGroupedItems.FirstOrDefault(x => x.Title == group.Title);
                bool insertGroup = filteredGroup == null;
                if (insertGroup)
                    filteredGroup = new LongListSelectorGroup<DatabaseItemViewModel>(group.Title);

                var filteredItemIndex = 0;
                foreach (var item in group)
                {
                    var isMatch = item.Matches(str);

                    var currentItemIndex = filteredGroup.IndexOf(item);
                    if (currentItemIndex == -1)
                    {
                        if (isMatch)
                            filteredGroup.Insert(filteredItemIndex, item);
                    }
                    else
                    {
                        if (isMatch)
                        {
                            for (var i = 0; i < (currentItemIndex - filteredItemIndex); i++)
                                filteredGroup.RemoveAt(0);
                        }
                        else
                        {
                            filteredGroup.RemoveAt(currentItemIndex);
                        }
                    }

                    if (isMatch)
                        filteredItemIndex++;
                }

                if (insertGroup)
                {
                    if (filteredGroup.Count > 0)
                        _filteredGroupedItems.Insert(filteredGroupIndex, filteredGroup);
                }
                else
                {
                    if (filteredGroup.Count == 0)
                        _filteredGroupedItems.Remove(filteredGroup);
                }
            }
        }

        private void Item_OnClick(object sender, RoutedEventArgs e)
        {
            var element = (FrameworkElement)sender;
            var item = (DatabaseItemViewModel)element.DataContext;

            DatabaseItemPage.NavigateTo(item);
        }

        //Pivot _pivot;

        //private void DatabasePivot_OnLoaded(object sender, RoutedEventArgs e)
        //{
        //    if (_loaded)
        //        return;

        //    _pivot = (Pivot) sender;
        //    var db = App.Safe;
        //    var groups = new Dictionary<string, List<Item>>();
        //    foreach (var item in db.Items)
        //    {
        //        var group = item.Group ?? "";
        //        if (!groups.ContainsKey(group))
        //            groups.Add(group, new List<Item>());
        //        groups[group].Add(item);
        //    }
        //    var groupNames = groups.Select(x => x.Key).ToList();
        //    groupNames.Sort();
        //    foreach (var groupName in groupNames)
        //    {
        //        var pivotItem = new PivotItem();
        //        pivotItem.Header = (groupName == "" ? "(no group)" : groupName);
        //        var groupControl = new DatabaseGroupUserControl();
        //        groupControl.DataContext = groups[groupName];
        //        pivotItem.Content = groupControl;
        //        _pivot.Items.Add(pivotItem);
        //    }

        //    _loaded = true;
        //}

        private void BuildApplicationBar()
        {
            // Set the page's ApplicationBar to a new instance of ApplicationBar.
            ApplicationBar = new ApplicationBar();

            // Create a new button and set the text value to the localized string from AppResources.
            //ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Icons/Save.png", UriKind.Relative));
            //appBarButton.Text = UbiquiKey.Resources.AppResources.SaveToLocal;
            //ApplicationBar.Buttons.Add(appBarButton);

            // Create a new menu item with the localized string from AppResources.
            ApplicationBarMenuItem closeDatabaseMenuItem = new ApplicationBarMenuItem(AppResources.CloseDatabaseMenuText);
            closeDatabaseMenuItem.Click += closeDatabaseMenuItem_Click;
            ApplicationBar.MenuItems.Add(closeDatabaseMenuItem);

            ApplicationBarMenuItem databaseInfoMenuItem = new ApplicationBarMenuItem(AppResources.DatabaseInfoMenuText);
            databaseInfoMenuItem.Click += databaseInfoMenuItem_Click;
            ApplicationBar.MenuItems.Add(databaseInfoMenuItem);

            ApplicationBarMenuItem aboutUbiquiKeyMenuItem = new ApplicationBarMenuItem(AppResources.AboutUbiquiKeyMenuText);
            aboutUbiquiKeyMenuItem.Click += aboutUbiquiKeyMenuItem_Click;
            ApplicationBar.MenuItems.Add(aboutUbiquiKeyMenuItem);
        }

        void closeDatabaseMenuItem_Click(object sender, EventArgs e)
        {
            App.Safe = null;
            App.LocalSafeFile = null;
            //App.RemoteSafeFile = null;

            //_pivot.Items.Clear();

            SafeOpenerPage.NavigateTo();
        }

        void databaseInfoMenuItem_Click(object sender, EventArgs e)
        {
            DatabaseInfoPage.NavigateTo();
        }

        void aboutUbiquiKeyMenuItem_Click(object sender, EventArgs e)
        {
            AboutPage.NavigateTo();
        }
    }
}
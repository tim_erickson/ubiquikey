﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using UbiquiKey.Annotations;
using System.Threading.Tasks;
using UbiquiKey.Resources;

namespace UbiquiKey.Pages
{
    public partial class LandingPage : SecurePage
    {
        //http://stackoverflow.com/questions/5119041/how-can-i-get-a-web-sites-favicon

        // Constructor
        public LandingPage()
        {
            App.TrackView("LandingPage");

            InitializeComponent();

            Loaded += OnLoaded;

            DataContext = this;

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private static readonly Uri Uri = new Uri(String.Format("/Pages/{0}.xaml", typeof(LandingPage).Name), UriKind.Relative);
        public static void NavigateTo()
        {
            GetPhoneApplicationFrame().Navigate(Uri);
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            Settings.Initialize();

            if (App.Safe != null)
            {
                if (App.IsLocked)
                {
                    Unlock();
                    return;
                }
                DatabasePage.NavigateTo();
                return;
            }

            if (App.LocalSafeFile != null)
            {
                Unlock();
                return;
            }

            SafeOpenerPage.NavigateTo();
        }

        private async void Unlock()
        {
            var unlocker = new DbUnlocker();
            //MessageBox.Show("Not Implemented: Unlock");
            var result = await unlocker.Show();
            if (result == ShowPopupResults.Success)
            {
                DatabasePage.NavigateTo();
                //} else if (/*max tries exceeded*/) {
                //    //clear/reset Db's
                //    FindDbOnSkyDrive();
            }
            else
            {
                //Unlock();
            }
        }

        // Sample code for building a localized ApplicationBar
        private void BuildLocalizedApplicationBar()
        {
            // Set the page's ApplicationBar to a new instance of ApplicationBar.
            ApplicationBar = new ApplicationBar();

            // Create a new button and set the text value to the localized string from AppResources.
            ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Icons/Save.png", UriKind.Relative));
            appBarButton.Text = AppResources.SaveToLocal;
            ApplicationBar.Buttons.Add(appBarButton);

            // Create a new menu item with the localized string from AppResources.
            //ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
            //ApplicationBar.MenuItems.Add(appBarMenuItem);
        }
    }
}
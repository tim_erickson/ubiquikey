﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Windows.System;
using PasswordSafe;
using UbiquiKey.ViewModels;
using UbiquiKey.Resources;
using System.Text.RegularExpressions;

namespace UbiquiKey.Pages
{
    public partial class DatabaseItemPage : SecurePage
    {
        public DatabaseItemPage()
        {
            App.TrackView("DatabaseItemPage");

            InitializeComponent();
        }

        private bool _hasUser;
        private bool _hasPassword;
        private bool _hasUrl;
        private bool _hasNotes;

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back)
                return;

            DataContext = this;

            _hasUser = _item.User != null;
            _hasPassword = _item.Password != null;
            _hasUrl = _item.Url != null;
            _hasNotes = _item.Notes != null;
            CopyUserIsEnabled = _hasUser;
            TogglePasswordIsEnabled = _hasPassword;
            TogglePasswordButtonContent = AppResources.DatabaseItemPageShowPasswordButtonContent;
            CopyPasswordIsEnabled = _hasPassword;
            TogglePasswordIsEnabled = _hasPassword;
            CopyPasswordAndLaunchUrlIsEnabled = _hasPassword && _hasUrl;
            ToggleNotesIsEnabled = _hasNotes;
            ToggleNotesButtonContent = AppResources.DatabaseItemPageShowNotesButtonContent;

            Title = _item.Title;
            User = _item.User;
            MaskingPassword = AppResources.DatabaseItemPageMaskedPasswordText;
            MaskingNotes = AppResources.DatabaseItemPageMaskedNotesText;
        }

        private static DatabaseItemViewModel _item;
        private static readonly Uri Uri = new Uri(String.Format("/Pages/{0}.xaml", typeof(DatabaseItemPage).Name), UriKind.Relative);
        public static void NavigateTo(DatabaseItemViewModel item)
        {
            _item = item;
            GetPhoneApplicationFrame().Navigate(Uri);
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set
            {
                if (String.Equals(_title, value))
                    return;
                _title = value;
                OnPropertyChanged();
            }
        }

        private string _user;
        public string User
        {
            get { return _user; }
            set
            {
                if (String.Equals(_user, value))
                    return;
                _user = value;
                OnPropertyChanged();
            }
        }

        private string _maskingPassword;
        public string MaskingPassword
        {
            get { return _maskingPassword; }
            set
            {
                if (String.Equals(_maskingPassword, value))
                    return;
                _maskingPassword = value;
                OnPropertyChanged();
            }
        }

        private string _maskingNotes;
        public string MaskingNotes
        {
            get { return _maskingNotes; }
            set
            {
                if (String.Equals(_maskingNotes, value))
                    return;
                _maskingNotes = value;
                OnPropertyChanged();
            }
        }

        private bool _copyUserIsEnabled = false;
        public bool CopyUserIsEnabled
        {
            get { return _copyUserIsEnabled; }
            set
            {
                if (Boolean.Equals(_copyUserIsEnabled, value))
                    return;
                _copyUserIsEnabled = value;
                OnPropertyChanged();
            }
        }

        private bool _togglePasswordIsEnabled = false;
        public bool TogglePasswordIsEnabled
        {
            get { return _togglePasswordIsEnabled; }
            set
            {
                if (Boolean.Equals(_togglePasswordIsEnabled, value))
                    return;
                _togglePasswordIsEnabled = value;
                OnPropertyChanged();
            }
        }

        private string _togglePasswordButtonContent;
        public string TogglePasswordButtonContent
        {
            get { return _togglePasswordButtonContent; }
            set
            {
                if (String.Equals(_togglePasswordButtonContent, value))
                    return;
                _togglePasswordButtonContent = value;
                OnPropertyChanged();
            }
        }

        private bool _copyPasswordIsEnabled = false;
        public bool CopyPasswordIsEnabled
        {
            get { return _copyPasswordIsEnabled; }
            set
            {
                if (Boolean.Equals(_copyPasswordIsEnabled, value))
                    return;
                _copyPasswordIsEnabled = value;
                OnPropertyChanged();
            }
        }

        private bool _copyPasswordAndLaunchUrlIsEnabled = false;
        public bool CopyPasswordAndLaunchUrlIsEnabled
        {
            get { return _copyPasswordAndLaunchUrlIsEnabled; }
            set
            {
                if (Boolean.Equals(_copyPasswordAndLaunchUrlIsEnabled, value))
                    return;
                _copyPasswordAndLaunchUrlIsEnabled = value;
                OnPropertyChanged();
            }
        }

        private bool _toggleNotesIsEnabled = false;
        public bool ToggleNotesIsEnabled
        {
            get { return _toggleNotesIsEnabled; }
            set
            {
                if (Boolean.Equals(_toggleNotesIsEnabled, value))
                    return;
                _toggleNotesIsEnabled = value;
                OnPropertyChanged();
            }
        }

        private string _toggleNotesButtonContent;
        public string ToggleNotesButtonContent
        {
            get { return _toggleNotesButtonContent; }
            set
            {
                if (String.Equals(_toggleNotesButtonContent, value))
                    return;
                _toggleNotesButtonContent = value;
                OnPropertyChanged();
            }
        }

        private void CopyUserName_OnClick(object sender, RoutedEventArgs e)
        {
            App.TrackEvent("ui-event", "db-item", "copy-user", 0);
            Clipboard.SetText(_item.User);
        }

        private void TogglePassword_OnClick(object sender, RoutedEventArgs e)
        {
            if (String.Equals(TogglePasswordButtonContent, AppResources.DatabaseItemPageShowPasswordButtonContent))
            {
                MaskingPassword = _item.Password;
                TogglePasswordButtonContent = AppResources.DatabaseItemPageHidePasswordButtonContent;
            }
            else
            {
                MaskingPassword = AppResources.DatabaseItemPageMaskedPasswordText;
                TogglePasswordButtonContent = AppResources.DatabaseItemPageShowPasswordButtonContent;
            }
        }

        private void CopyPassword_OnClick(object sender, RoutedEventArgs e)
        {
            App.TrackEvent("ui-event", "db-item", "copy-password", 0);
            Clipboard.SetText(_item.Password);
        }

        private async void CopyPasswordAndLaunchUrl_OnClick(object sender, RoutedEventArgs e)
        {
            App.TrackEvent("ui-event", "db-item", "copy-password-launch", 0);
            Clipboard.SetText(_item.Password);
            //JK: Got System.UriFormatException RTE here because I was using the "[alt]" prefix feature to launch an alternative browser. Might be useful on a phone but 
            // for now I'll just strip it.
            var strippedUrl = Regex.Replace(_item.Url, "^\\[alt\\]", string.Empty);
            await Launcher.LaunchUriAsync(new Uri(strippedUrl));
        }

        private void ToggleNotes_OnClick(object sender, RoutedEventArgs e)
        {
            if (String.Equals(ToggleNotesButtonContent, AppResources.DatabaseItemPageShowNotesButtonContent))
            {
                MaskingNotes = _item.Notes;
                ToggleNotesButtonContent = AppResources.DatabaseItemPageHideNotesButtonContent;
            }
            else
            {
                MaskingNotes = AppResources.DatabaseItemPageMaskedNotesText;
                ToggleNotesButtonContent = AppResources.DatabaseItemPageShowNotesButtonContent;
            }
        }
    }
}
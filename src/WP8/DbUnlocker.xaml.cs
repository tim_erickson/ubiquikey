﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PasswordSafe;
using PasswordSafe.Crypto;
using UbiquiKey.Annotations;
using UbiquiKey.Pages;

namespace UbiquiKey
{
    public partial class DbUnlocker : PopupUserControl, INotifyPropertyChanged
    {
        public DbUnlocker()
        {
            InitializeComponent();

            DataContext = this;
        }

        private Stream _protectedDbStream;
        private Stream _dbStream;

        private int _lockoutCountdown;
        public int LockoutCountdown
        {
            get { return _lockoutCountdown; }
            set
            {
                if (_lockoutCountdown == value)
                    return;
                _lockoutCountdown = value;
                OnPropertyChanged();
            }
        }

        private Visibility _lockoutCountdownVisibility = Visibility.Visible;
        public Visibility LockoutCountdownVisibility
        {
            get { return _lockoutCountdownVisibility; }
            set
            {
                if (_lockoutCountdownVisibility == value)
                    return;
                _lockoutCountdownVisibility = value;
                OnPropertyChanged();
            }
        }

        private Visibility _lockoutMessageVisibility = Visibility.Collapsed;
        public Visibility LockoutMessageVisibility
        {
            get { return _lockoutMessageVisibility; }
            set
            {
                if (_lockoutMessageVisibility == value)
                    return;
                _lockoutMessageVisibility = value;
                OnPropertyChanged();
            }
        }

        private bool _pinPadIsEnabled = true;
        public bool PinPadIsEnabled
        {
            get { return _pinPadIsEnabled; }
            set
            {
                if (_pinPadIsEnabled == value)
                    return;
                _pinPadIsEnabled = value;
                OnPropertyChanged();
            }
        }

        private string _dbName;
        public string DbName
        {
            get { return _dbName; }
            set
            {
                if (string.Equals(value, _dbName))
                    return;
                _dbName = value;
                OnPropertyChanged();
            }
        }

        private string _pin;
        public string Pin
        {
            get { return _pin; }
            set
            {
                if (string.Equals(value, _pin))
                    return;
                _pin = value;
                OnPropertyChanged();
            }
        }

        private ICommand _enterCommand;
        public ICommand EnterCommand
        {
            get { return _enterCommand ?? (_enterCommand = new SimpleDelegateCommand(ExecuteEnterCommand)); }
        }

        public override Task<ShowPopupResults> Show()
        {
            if (App.LocalSafeFile == null)
                throw new Exception("No Local file to unlock");
            
            return base.Show();
        }

        async protected override void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            using (var storage = IsolatedStorageFile.GetUserStoreForApplication())
                _protectedDbStream = storage.OpenFile(App.LocalSafeFile, FileMode.Open);
            var protectedBytes = new byte[_protectedDbStream.Length];
            await _protectedDbStream.ReadAsync(protectedBytes, 0, protectedBytes.Length);
            var dbBytes = ProtectedData.Unprotect(protectedBytes, null);
            _dbStream = new MemoryStream(dbBytes);

            LockoutCountdown = Settings.UnlockFailuresAllowed - Settings.UnlockFailures;
            if (LockoutCountdown >= 0)
            {
                LockoutCountdownVisibility = Visibility.Visible;
                LockoutMessageVisibility = Visibility.Collapsed;
            }
            else
            {
                LockoutCountdownVisibility = Visibility.Collapsed;
                LockoutMessageVisibility = Visibility.Visible;
            }
        }

        private void ExecuteEnterCommand(object parameter)
        {
            bool unlockFailed = false;
            try
            {
                var db = Safe.Load(new PasswordSafeCrypto(), _dbStream, Pin);
                App.Safe = db;
                Close(ShowPopupResults.Success);
            }
            catch
            {
                unlockFailed = true;
            }

            if (unlockFailed)
            {
                LockoutCountdown--;
                Settings.UnlockFailures++;
                if (LockoutCountdown >= 0)
                {
                    MessageBox.Show("Failed to open Password Safe.  Please try again.");
                } 
                else
                {
                    Lockout();
                }
            }
        }

        async private void Lockout()
        {
            LockoutCountdownVisibility = Visibility.Collapsed;
            LockoutMessageVisibility = Visibility.Visible;
            PinPadIsEnabled = false;
            App.Lockout();
            await Task.Delay(5000);
            Close(ShowPopupResults.Failure);
            SafeOpenerPage.NavigateTo();
        }

        private void Open_OnCompleted(object sender, EventArgs e)
        {
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using UbiquiKey.Annotations;

namespace UbiquiKey
{
    public class PopupUserControl : UserControl, INotifyPropertyChanged
    {
        public PopupUserControl()
        {
            Loaded += OnLoaded;
        }

        private ShowPopupResults? _result;

        private readonly Popup _popup = new Popup();
        public virtual Task<ShowPopupResults> Show()
        {
            if (_popup.IsOpen || _result.HasValue)
                throw new Exception("Can't show Popup twice");
            var taskCompletionSource = new TaskCompletionSource<ShowPopupResults>();
            const double topMargin = 0;
            var width = Application.Current.Host.Content.ActualWidth;
            var height = Application.Current.Host.Content.ActualHeight - topMargin;
            _popup.Width = width;
            _popup.Height = height;
            Width = width;
            Height = height;
            //Canvas.SetTop(this, topMargin);
            _popup.Child = this;
            _popup.Closed += delegate(object sender, EventArgs args)
            {
                _result = _result ?? ShowPopupResults.Failure;
                taskCompletionSource.SetResult(_result.Value);
            };
            _popup.IsOpen = true;
            return taskCompletionSource.Task;
        }

        protected virtual void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            
        }

        public void Close(ShowPopupResults result)
        {
            if (!_popup.IsOpen)
                throw new Exception("Can't be closed when it is not Open");
            _result = result;
            _popup.IsOpen = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public enum ShowPopupResults
    {
        Success,
        Failure
    }
}

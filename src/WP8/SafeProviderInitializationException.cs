﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UbiquiKey
{
    class SafeProviderInitializationException : Exception
    {
        public SafeProviderInitializationException(string message) : base(message)
        {
        }
    }
}

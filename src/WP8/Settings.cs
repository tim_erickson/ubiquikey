﻿using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbiquiKey
{
    class Settings
    {
        private static IsolatedStorageSettings ApplicationSettings = IsolatedStorageSettings.ApplicationSettings;

        internal static void Initialize()
        {
            if (AreSettingsInitialized)
                return;

            if (!ApplicationSettings.Contains(IsAnalyticsAllowedKey)) IsAnalyticsAllowed = DefaultIsAnalyticsAllowed;
            if (!ApplicationSettings.Contains(UnlockFailuresAllowedKey)) UnlockFailuresAllowed = DefaultUnlockFailuresAllowed;
            if (!ApplicationSettings.Contains(UnlockFailuresKey)) UnlockFailures = DefaultUnlockFailures;

            AreSettingsInitialized = true;
        }

        private const string AreSettingsInitializedKey = "AreSettingsInitialized";
        internal static bool AreSettingsInitialized
        {
            get { return Get<bool>(AreSettingsInitializedKey); }
            set { Set(AreSettingsInitializedKey, value); }
        }

        private const bool DefaultIsAnalyticsAllowed = false;
        private const string IsAnalyticsAllowedKey = "IsAnalyticsAllowed";
        internal static bool? IsAnalyticsAllowed
        {
            get { return Get<bool?>(IsAnalyticsAllowedKey); }
            set { Set(IsAnalyticsAllowedKey, value); }
        }

        private const int DefaultUnlockFailuresAllowed = 2;
        private const string UnlockFailuresAllowedKey = "UnlockFailuresAllowed";
        internal static int UnlockFailuresAllowed
        {
            get { return Get<int>(UnlockFailuresAllowedKey); }
            set { Set(UnlockFailuresAllowedKey, value); }
        }

        private const int DefaultUnlockFailures = 0;
        private const string UnlockFailuresKey = "UnlockFailures";
        internal static int UnlockFailures
        {
            get { return Get<int>(UnlockFailuresKey); }
            set { Set(UnlockFailuresKey, value); }
        }

        internal static TValue Get<TValue>(string key)
        {
            if (!ApplicationSettings.Contains(key))
                return default(TValue);
            return (TValue)ApplicationSettings[key];
        }

        internal static void Set<TValue>(string key, TValue value)
        {
            ApplicationSettings[key] = value;
            ApplicationSettings.Save();
        }
    }
}

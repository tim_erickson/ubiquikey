﻿using Microsoft.Live;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;
using UbiquiKey.Models;
using UbiquiKey.Pages;

namespace UbiquiKey.OneDrive
{
    class OneDriveSafeProvider : ISafeProvider
    {
        const string ProviderName = "OneDrive";

        const string ClientId = "00000000401527B4";
        const string ClientSecret = "bOYKS5RjYeOJYjfQwyacDtPhgJpEEAqO";
        const string RedirectUri = "http://ubiquikey.in2bits.com/redirect";

        private LiveAuthClient _authClient;
        private LiveConnectSession _session;
        private LiveConnectClient _client;

        string ISafeProvider.GetName()
        {
            return ProviderName;
        }

        async Task<Stream> ISafeProvider.GetStream(ISafeInfo safeInfo)
        {
            var skyDriveItem = safeInfo as SkyDriveItem;
            if (skyDriveItem == null)
                throw new ArgumentException("must be a OneDriveItem", "safeInfo");

            var loResult = await _client.DownloadAsync(skyDriveItem.UploadLocation);
            return loResult.Stream;
        }


        async Task<IEnumerable<ISafeInfo>> ISafeProvider.SearchForSafes()
        {
            var json = (await _client.GetAsync("/me/skydrive/search?q=.psafe3")).RawResult;

            var result = JsonConvert.DeserializeObject<SkyDriveSearchResults>(json);

            var skyDriveItems = result.Data.ToList();

            Dictionary<string, string> folderNames = new Dictionary<string, string>();

            foreach (var dbFile in skyDriveItems.ToArray())
            {
                if (!dbFile.Name.EndsWith(".psafe3"))
                {
                    skyDriveItems.Remove(dbFile);
                    continue;
                }
                if (!folderNames.ContainsKey(dbFile.ParentId))
                {
                    var folderJson = (await _client.GetAsync(String.Format("/{0}", dbFile.ParentId))).RawResult;
                    var folder = JsonConvert.DeserializeObject<SkyDriveItem>(folderJson);
                    folderNames.Add(dbFile.ParentId, folder.Name);
                }
                dbFile.Path = folderNames[dbFile.ParentId];
            }

            return skyDriveItems;
        }

        async Task<ISafeInfo> ISafeProvider.FindSafe(NavigationService navigationService)
        {
            var result = await SafePickerPage.Open(navigationService, this);
            return result;
        }

        async Task ISafeProvider.Initialize(NavigationService navigationService)
        {
            if (_session != null)
                return;

            _authClient = new LiveAuthClient(ClientId);
            var scopes = new[] { "wl.signin", "wl.offline_access", "wl.skydrive" };
            var result = await _authClient.InitializeAsync(scopes);
            if (result.Status == LiveConnectSessionStatus.Connected)
            {
                _session = result.Session;
            }
            else
            {
                result = await _authClient.LoginAsync(scopes);
                if (result.Status == LiveConnectSessionStatus.Connected)
                    _session = result.Session;
            }
            if (_session == null)
            {
                throw new SafeProviderInitializationException("Failed to connect to OneDrive");
            }
            Debug.WriteLine("Expires at " + _session.Expires.ToLocalTime());
            _client = new LiveConnectClient(_session);
        }

        async Task ISafeProvider.Uninitialize()
        {
            _client = null;
            _session = null;
            _authClient = null;
        }

    }
}

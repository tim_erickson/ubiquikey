﻿using System.Runtime.Serialization;

namespace UbiquiKey.Models
{
    [DataContract]
    public class SkyDriveAuthor
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }
    }
}
﻿using System.Runtime.Serialization;

namespace UbiquiKey.Models
{
    [DataContract]
    public class SkyDriveSharing
    {
        [DataMember(Name = "access")]
        public string Access { get; set; }
    }
}
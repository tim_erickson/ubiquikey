﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace UbiquiKey.Models
{
    [DataContract]
    public class SkyDriveSearchResults
    {
        [DataMember(Name = "data")]
        public IEnumerable<SkyDriveItem> Data { get; set; } 
    }
}
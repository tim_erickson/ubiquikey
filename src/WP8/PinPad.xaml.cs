﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using UbiquiKey.Annotations;

namespace UbiquiKey
{
    public partial class PinPadControl : UserControl, INotifyPropertyChanged
    {
        public PinPadControl()
        {
            InitializeComponent();

            LayoutRoot.DataContext = this;
        }

        public static DependencyProperty PromptProperty = DependencyProperty.Register(
            "Prompt", typeof (string), typeof (PinPadControl), new PropertyMetadata(PromptChanged));

        public static DependencyProperty PinProperty = DependencyProperty.Register(
            "Pin", typeof (string), typeof (PinPadControl), new PropertyMetadata(PinChanged));

        public static DependencyProperty EnterCommandProperty = DependencyProperty.Register(
            "EnterCommand", typeof(ICommand), typeof(PinPadControl), null);

        private static void PromptChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var instance = (PinPadControl)d;
            instance.InternalPrompt = instance.Prompt;
        }

        private static void PinChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var instance = (PinPadControl) d;
            instance.InternalPin = instance.Pin;
        }

        public string Prompt
        {
            get { return (string)GetValue(PromptProperty); }
            set { SetValue(PromptProperty, value); }
        }

        public string Pin
        {
            get { return (string) GetValue(PinProperty); }
            set { SetValue(PinProperty, value); }
        }

        public ICommand EnterCommand
        {
            get { return (ICommand)GetValue(EnterCommandProperty); }
            set { SetValue(EnterCommandProperty, value); }
        }

        private ICommand _pinKeyCommand;
        public ICommand PinKeyCommand
        {
            get { return _pinKeyCommand ?? (_pinKeyCommand = new SimpleDelegateCommand(ExecutePinKeyCommand)); }
        }

        private void ExecutePinKeyCommand(object parameter)
        {
            var s = (string) parameter;
            InternalPin += s;
        }

        private ICommand _backKeyCommand;
        public ICommand BackKeyCommand
        {
            get { return _backKeyCommand ?? (_backKeyCommand = new SimpleDelegateCommand(ExecuteBackKeyCommand)); }
        }

        private void ExecuteBackKeyCommand(object parameter)
        {
            if (InternalPin == null) // JK
                return;
            if (InternalPin.Length == 0)
                return;
            InternalPin = InternalPin.Substring(0, InternalPin.Length - 1);
            if (InternalPin.Length == 0)
                this.EnterPromptVisibility = Visibility.Visible;
        }

        private string _internalPrompt;
        public string InternalPrompt
        {
            get { return _internalPrompt; }
            set
            {
                if (string.Equals(value, _internalPrompt))
                    return;
                _internalPrompt = value;
                OnPropertyChanged();
            }
        }

        private string _internalPin;
        public string InternalPin
        {
            get { return _internalPin; }
            set
            {
                if (string.Equals(value, _internalPin))
                    return;
                _internalPin = value;
                OnPropertyChanged();
                Pin = value;

                EnterPromptVisibility = string.IsNullOrEmpty(_internalPin) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        private ICommand _internalEnterCommand;
        public ICommand InternalEnterCommand
        {
            get { return _internalEnterCommand ?? (_internalEnterCommand = new SimpleDelegateCommand(ExecuteInternalEnterCommand)); }
        }

        private void ExecuteInternalEnterCommand(object obj)
        {
            if (EnterCommand == null)
                return;
            EnterCommand.Execute(obj);
        }

        private Visibility _enterPromptVisibility;
        public Visibility EnterPromptVisibility
        {
            get { return _enterPromptVisibility; }
            set
            {
                if (value == _enterPromptVisibility)
                    return;
                _enterPromptVisibility = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

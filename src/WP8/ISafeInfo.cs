﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbiquiKey
{
    public interface ISafeInfo
    {
        string Name { get; }
    }
}

﻿using DropNetRT;
using DropNetRT.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;
using UbiquiKey.Pages;

namespace UbiquiKey.DropBox
{
    class DropBoxSafeProvider : ISafeProvider
    {
        //https://www.dropbox.com/developers/core/docs

        const string ProviderName = "OneDrive";

        const string StorageKey = "DropBoxToken";

        const string key = "pc703mka8s0l59q";
        const string secret = "72h8br0b06pswcc";

        DropNetClient _client;

        string ISafeProvider.GetName()
        {
            return ProviderName;
        }

        async Task ISafeProvider.Initialize(NavigationService navigationService)
        {
            if (_client != null)
                return;

            AppUserLogin appUserLogin;

            var settings = IsolatedStorageSettings.ApplicationSettings;
            if (settings.Contains(StorageKey))
            {
                var accessTokenJson = (string)settings[StorageKey];
                appUserLogin = JsonConvert.DeserializeObject<AppUserLogin>(accessTokenJson);
            }
            else
            {
                var authClient = new DropNetClient(key, secret);

                var tempToken = await authClient.GetRequestToken();

                var authUriString = authClient.BuildAuthorizeUrl(authClient.UserLogin);

                IOAuthHandler handler = new DropBoxOAuthHandler();
                await handler.Start(navigationService, new Uri(authUriString, UriKind.Absolute));

                appUserLogin = AppUserLogin.From(await authClient.GetAccessToken());
                var accessTokenJson = JsonConvert.SerializeObject(appUserLogin);
                settings[StorageKey] = accessTokenJson;
                settings.Save();
            }

            _client = new DropNetClient(key, secret, appUserLogin.Token, appUserLogin.Secret);
        }

        async Task ISafeProvider.Uninitialize()
        {
            _client = null;
        }

        async Task<IEnumerable<ISafeInfo>> ISafeProvider.SearchForSafes()
        {
            if (_client == null)
                throw new SafeProviderInitializationException("Not initialized");
            var infos = (await _client.Search(".psafe3")).Select(x => (ISafeInfo)new DropBoxSafeInfo(x));
            return infos;
        }

        async Task<System.IO.Stream> ISafeProvider.GetStream(ISafeInfo safeInfo)
        {
            if (_client == null)
                throw new SafeProviderInitializationException("Not initialized");
            var dropBoxSafeInfo = safeInfo as DropBoxSafeInfo;
            if (dropBoxSafeInfo == null)
                throw new ArgumentException("must be a DropBoxSafeInfo", "safeInfo");
            return await _client.GetFileStream(dropBoxSafeInfo.Metadata.Path);
        }

        async Task<ISafeInfo> ISafeProvider.FindSafe(System.Windows.Navigation.NavigationService navigationService)
        {
            return await SafePickerPage.Open(navigationService, this);
        }
    }
}

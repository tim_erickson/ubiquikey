﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;
using UbiquiKey.Pages;

namespace UbiquiKey.DropBox
{
    class DropBoxOAuthHandler : IOAuthHandler
    {
        const string PermissionSubmissionUriFragment = "authorize_submit";

        private TaskCompletionSource<bool> _completionSource;
        private IOAuthUI _oauthUi;
        private bool _permissionSubmitted;
        private bool _allowed;

        Task IOAuthHandler.Start(NavigationService navigationService, Uri authUri)
        {
            _completionSource = new TaskCompletionSource<bool>();
            OAuthPage.Show(navigationService, this, authUri);
            return _completionSource.Task;
        }

        void IOAuthHandler.RegisterOAuthUI(IOAuthUI oauthUi)
        {
            _oauthUi = oauthUi;
        }

        void IOAuthHandler.Navigating(Uri uri)
        {
            Debug.WriteLine("Navigating: " + uri);
            if (uri.OriginalString.Contains(PermissionSubmissionUriFragment))
            {
                _permissionSubmitted = true;
            }
        }

        void IOAuthHandler.Navigated(Uri uri)
        {
            /*
             * Navigating: https://api.dropbox.com/1/oauth/authorize?oauth_token=iQyP84WjNvzEC8my
             * Navigating: https://www.dropbox.com/1/oauth/authorize?oauth_token=iQyP84WjNvzEC8my
             * Navigated: https://www.dropbox.com/1/oauth/authorize?oauth_token=iQyP84WjNvzEC8my
             * 
             * Navigating: https://www.dropbox.com/1/oauth/authorize_submit
             * 
             * (correct + allow)
             * Navigated: https://www.dropbox.com/1/oauth/authorize_submit
             * 
             * (correct + deny)
             * Navigating: https://www.dropbox.com/
             * Navigating: https://www.dropbox.com/home
             * Navigated: https://www.dropbox.com/home
             */
            Debug.WriteLine("Navigated: " + uri);
            if (_permissionSubmitted)
            {
                if (uri.OriginalString.Contains(PermissionSubmissionUriFragment))
                {
                    _allowed = true;
                }
                _oauthUi.Close();
                _completionSource.SetResult(_allowed);
            }
        }

        void IOAuthHandler.NavigationFailed(Uri uri)
        {
            _completionSource.SetResult(false);
        }
    }
}

﻿using DropNetRT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UbiquiKey.DropBox
{
    [DataContract]
    public class AppUserLogin
    {
        [DataMember]
        public string Token { get; set; }

        [DataMember]
        public string Secret { get; set; }

        public static AppUserLogin From(UserLogin login)
        {
            return new AppUserLogin
            {
                Token = login.Token,
                Secret = login.Secret
            };
        }
    }
}

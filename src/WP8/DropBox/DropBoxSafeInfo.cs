﻿using DropNetRT.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbiquiKey.DropBox
{
    public class DropBoxSafeInfo : ISafeInfo
    {
        private readonly Metadata _metadata;

        public DropBoxSafeInfo(Metadata metadata)
        {
            _metadata = metadata;
        }

        public string Name
        {
            get { return _metadata.Name; }
        }

        public string Path
        {
            get { return _metadata.Path; }
        }

        internal Metadata Metadata
        {
            get { return _metadata; }
        }
    }
}

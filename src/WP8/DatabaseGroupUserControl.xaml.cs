﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PasswordSafe;
using UbiquiKey.Pages;
using UbiquiKey.ViewModels;

namespace UbiquiKey
{
    public partial class DatabaseGroupUserControl : UserControl
    {
        public DatabaseGroupUserControl()
        {
            InitializeComponent();
        }

        private void Item_OnClick(object sender, RoutedEventArgs e)
        {
            var element = (FrameworkElement) sender;
            var item = (DatabaseItemViewModel) element.DataContext;

            DatabaseItemPage.NavigateTo(item);
        }
    }
}

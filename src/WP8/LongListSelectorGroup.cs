﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UbiquiKey
{
    public class LongListSelectorGroup<TItem> : ObservableCollection<TItem>
    {
        public LongListSelectorGroup(string title, IEnumerable<TItem> items) : base(items)
        {
            Title = title;
        }

        public LongListSelectorGroup(string title) : this(title, new List<TItem>())
        {
        }

        public string Title { get; set; }
    }
}

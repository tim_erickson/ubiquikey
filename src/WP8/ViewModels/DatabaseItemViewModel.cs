﻿using PasswordSafe;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using UbiquiKey.Annotations;

namespace UbiquiKey.ViewModels
{
    public class DatabaseItemViewModel : ViewModelBase
    {
        private readonly Item _item;

        public DatabaseItemViewModel(Item item)
        {
            _item = item;
        }

        public DateTime AccessedTime { get { return _item.AccessedTime; } }
        public string AutoType { get { return _item.AutoType; } }
        public DateTime CreatedTime { get { return _item.CreatedTime; } }
        public string Group { get { return _item.Group; } }
        public DateTime ModifiedTime { get { return _item.ModifiedTime; } }
        public string Notes { get { return _item.Notes; } }
        public string Password { get { return _item.Password; } }
        public DateTime PasswordExpires { get { return _item.PasswordExpires; } }
        public DateTime PasswordModified { get { return _item.PasswordModified; } }
        public string Title { get { return _item.Title; } }
        public string Url { get { return _item.Url; } }
        public string User { get { return _item.User; } }
        public Guid Uuid { get { return _item.Uuid; } }

        public class TitleComparer : IComparer<DatabaseItemViewModel>
        {
            int IComparer<DatabaseItemViewModel>.Compare(DatabaseItemViewModel x, DatabaseItemViewModel y)
            {
                if (x == y)
                    return 0;
                if (x == null)
                    return -1;
                if (y == null)
                    return 1;
                return String.Compare(x.Title, y.Title);
            }
        }

        public bool Matches(string str)
        {
            return StringContains(Title, str) ||
                StringContains(Title, str) ||
                StringContains(User, str) ||
                StringContains(Notes, str) ||
                StringContains(Password, str) ||
                StringContains(Url, str)/* ||
                StringContains(PasswordHistory, str) ||
                StringContains(Email, str)*/;
        }

        private bool StringContains(string str, string contains)
        {
            if (contains == null)
                return true;
            if (str == null)
                return false;
            return str.IndexOf(contains, StringComparison.CurrentCultureIgnoreCase) >= 0;
        }
    }
}

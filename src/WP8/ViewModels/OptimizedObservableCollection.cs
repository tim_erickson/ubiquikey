﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using UbiquiKey.Annotations;

namespace UbiquiKey.ViewModels
{
    public class OptimizedObservableCollection<TItem> : IList<TItem>, ICollection<TItem>, IReadOnlyList<TItem>, IReadOnlyCollection<TItem>, IEnumerable<TItem>, IList, ICollection, IEnumerable,
        INotifyCollectionChanged, INotifyPropertyChanged
    {
        private readonly ObservableCollection<TItem> _collection = new ObservableCollection<TItem>();

        private bool _suppressEvents = false;

        public OptimizedObservableCollection() : this(null)
        {
        }

        public OptimizedObservableCollection(IEnumerable<TItem> items)
        {
            if (items != null)
            {

            }

            _collection.CollectionChanged += _collection_CollectionChanged;
        }

        void _collection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (_suppressEvents)
                return;

            var handler = CollectionChanged;
            if (handler == null)
                return;
            NotifyCollectionChangedEventArgs args;
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    {
                        args = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, e.NewItems);
                        break;
                    }
                case NotifyCollectionChangedAction.Move:
                    {
                        args = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Move, e.OldItems, e.OldStartingIndex, e.NewStartingIndex);
                        break;
                    }
                case NotifyCollectionChangedAction.Remove:
                    {
                        args = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, e.OldItems);
                        break;
                    }
                case NotifyCollectionChangedAction.Replace:
                    {
                        args = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, e.NewItems, e.OldItems);
                        break;
                    }
                case NotifyCollectionChangedAction.Reset:
                    {
                        args = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
                        break;
                    }
                default:
                    throw new NotSupportedException(String.Format("NotifyCollectionChangedAction {0}", e.Action));
            }
            handler(this, args);
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        int IList<TItem>.IndexOf(TItem item)
        {
            return _collection.IndexOf(item);
        }

        void IList<TItem>.Insert(int index, TItem item)
        {
            _collection.Insert(index, item);
        }

        void IList<TItem>.RemoveAt(int index)
        {
            _collection.RemoveAt(index);
        }

        TItem IList<TItem>.this[int index]
        {
            get
            {
                return _collection[index];
            }
            set
            {
                _collection[index] = value;
            }
        }

        void ICollection<TItem>.Add(TItem item)
        {
            _collection.Add(item);
        }

        void ICollection<TItem>.Clear()
        {
            _collection.Clear();
        }

        bool ICollection<TItem>.Contains(TItem item)
        {
            return _collection.Contains(item);
        }

        void ICollection<TItem>.CopyTo(TItem[] array, int arrayIndex)
        {
            _collection.CopyTo(array, arrayIndex);
        }

        int ICollection<TItem>.Count
        {
            get { return _collection.Count; }
        }

        bool ICollection<TItem>.IsReadOnly
        {
            get { return ((ICollection<TItem>)_collection).IsReadOnly; }
        }

        bool ICollection<TItem>.Remove(TItem item)
        {
            return _collection.Remove(item);
        }

        IEnumerator<TItem> IEnumerable<TItem>.GetEnumerator()
        {
            return _collection.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _collection.GetEnumerator();
        }

        TItem IReadOnlyList<TItem>.this[int index]
        {
            get { return _collection[index]; }
        }

        int IReadOnlyCollection<TItem>.Count
        {
            get { return _collection.Count; }
        }

        int IList.Add(object value)
        {
            var item = (TItem)value;
            _collection.Add(item);
            return _collection.IndexOf(item);
        }

        void IList.Clear()
        {
            _collection.Clear();
        }

        bool IList.Contains(object value)
        {
            return _collection.Contains((TItem)value);
        }

        int IList.IndexOf(object value)
        {
            return _collection.IndexOf((TItem)value);
        }

        void IList.Insert(int index, object value)
        {
            _collection.Insert(index, (TItem)value);
        }

        bool IList.IsFixedSize
        {
            get { return ((IList)_collection).IsFixedSize; }
        }

        bool IList.IsReadOnly
        {
            get { return ((IList)_collection).IsReadOnly; }
        }

        void IList.Remove(object value)
        {
            _collection.Remove((TItem)value);
        }

        void IList.RemoveAt(int index)
        {
            _collection.RemoveAt(index);
        }

        object IList.this[int index]
        {
            get
            {
                return _collection[index];
            }
            set
            {
                _collection[index] = (TItem)value;
            }
        }

        void ICollection.CopyTo(Array array, int index)
        {
            Array.Copy(_collection.ToArray(), array, array.Length - index);
        }

        int ICollection.Count
        {
            get { return ((ICollection)_collection).Count; }
        }

        bool ICollection.IsSynchronized
        {
            get { return ((ICollection)_collection).IsSynchronized; }
        }

        object ICollection.SyncRoot
        {
            get { return ((ICollection)_collection).SyncRoot; }
        }
    }
}

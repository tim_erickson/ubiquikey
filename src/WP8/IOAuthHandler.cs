﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace UbiquiKey
{
    public interface IOAuthHandler
    {
        Task Start(NavigationService navigationService, Uri authUri);
        void RegisterOAuthUI(IOAuthUI ui);
        void Navigating(Uri uri);
        void Navigated(Uri uri);
        void NavigationFailed(Uri uri);
    }
}

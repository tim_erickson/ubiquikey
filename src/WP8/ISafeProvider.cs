﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Navigation;
using UbiquiKey.DropBox;
using UbiquiKey.OneDrive;

namespace UbiquiKey
{
    public interface ISafeProvider
    {
        string GetName();
        Task Initialize(NavigationService navigationService);
        Task Uninitialize();
        Task<IEnumerable<ISafeInfo>> SearchForSafes();
        Task<Stream> GetStream(ISafeInfo safeInfo);
        Task<ISafeInfo> FindSafe(NavigationService navigationService);
    }

    static class SafeProviders
    {
        private static ISafeProvider _oneDrive = new OneDriveSafeProvider();
        public static ISafeProvider OneDrive { get { return _oneDrive; } }

        private static ISafeProvider _dropBox = new DropBoxSafeProvider();
        public static ISafeProvider DropBox { get { return _dropBox; } }

        public static IReadOnlyCollection<ISafeProvider> All = new List<ISafeProvider>
        {
            OneDrive,
            DropBox
        };
    }
}
